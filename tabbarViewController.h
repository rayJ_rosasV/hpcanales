//
//  tabbarViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 07/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tabbarViewController : UITabBarController

@property (strong, nonatomic) NSString* opcionInicio;
@property (weak, nonatomic) IBOutlet UITabBar *tab;

@end
