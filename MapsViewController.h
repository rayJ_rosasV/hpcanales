//
//  MapsViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 10/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface MapsViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet MKMapView *vistaMapa;
@property (weak, nonatomic) IBOutlet UISwitch *botonloc;
- (IBAction)comoLlegar:(UIButton *)sender;
- (IBAction)botonmapa:(UISegmentedControl *)sender;
- (IBAction)localizacion:(UISwitch *)sender;
- (IBAction)Atras:(UIButton *)sender;

@end
