//
//  menuInicioViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 22/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface menuInicioViewController : UIViewController
@property (strong, nonatomic) NSString* opcion;
- (IBAction)canales:(UIButton *)sender;
- (IBAction)configurador:(UIButton *)sender;
- (IBAction)eventos:(UIButton *)sender;
- (IBAction)promos:(UIButton *)sender;
- (IBAction)dondeComprar:(UIButton *)sender;
- (IBAction)articulos:(UIButton *)sender;

@end
