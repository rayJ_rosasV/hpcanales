//
//  menuInicioViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 22/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "menuInicioViewController.h"
#import "tabbarViewController.h"

@interface menuInicioViewController ()

@end

@implementation menuInicioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)canales:(UIButton *)sender {
    _opcion = @"canales";
}

- (IBAction)configurador:(UIButton *)sender {
    _opcion = @"configurador";
}

- (IBAction)eventos:(UIButton *)sender {
    _opcion = @"eventos";

}

- (IBAction)promos:(UIButton *)sender {
    _opcion = @"promos";

}

- (IBAction)dondeComprar:(UIButton *)sender {
    _opcion = @"dondeComprar";

}

- (IBAction)articulos:(UIButton *)sender {
    _opcion = @"articulos";

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    tabbarViewController* DestinationController = segue.destinationViewController;
    DestinationController.opcionInicio = _opcion;
    
}
@end
