//
//  DetalleEventosViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 06/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "mail.h"


@interface DetalleEventosViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) NSString* dirPagina;
@property (strong, nonatomic) NSString* titulo;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *botonConfirmNO;
@property (weak, nonatomic) IBOutlet UIButton *botonConfirmYES;
@property (strong, nonatomic) MFMailComposeViewController *mailComposer;
@property (weak, nonatomic) IBOutlet UILabel *texto;

- (IBAction)ConfirmNO:(UIButton *)sender;
- (IBAction)ConfirmYEs:(UIButton *)sender;
- (IBAction)ayuda:(UIBarButtonItem *)sender;

@end
