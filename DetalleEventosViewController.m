//
//  DetalleEventosViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 06/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "DetalleEventosViewController.h"

@interface DetalleEventosViewController ()

@end

@implementation DetalleEventosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    //    NSString* jQuey=[[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"html" encoding:NSUTF8StringEncoding error:nil];
    _botonConfirmNO.backgroundColor=[UIColor redColor];
    _botonConfirmYES.backgroundColor=[UIColor greenColor];
    
    _texto.text = _titulo;
    NSURL* jqueryURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"jquery" ofType:@"html"]];
    NSString* jqueryContent = [NSString stringWithContentsOfURL:jqueryURL encoding:NSUTF8StringEncoding error:nil];
    
    NSURL* siteURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"dir" ofType:@"html"]];
    NSString* dirContent = [NSString stringWithContentsOfURL:siteURL encoding:NSUTF8StringEncoding error:nil];
    
    
    NSMutableString* direccionHTML= [NSMutableString stringWithString:dirContent];
    //NSLog(@"dirpag= %@",_dirPagina);
    
    [direccionHTML appendString:_dirPagina];
    //NSLog(@"dirnuevapag= %@",direccionHTML);
    
    NSURL *url = [[NSURL alloc] initWithString:direccionHTML];
    
    NSMutableString* contenidoHTML= [NSMutableString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    [contenidoHTML appendString:jqueryContent];
    


   [_webView loadHTMLString:contenidoHTML baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ConfirmNO:(UIButton *)sender {
}

- (IBAction)ConfirmYEs:(UIButton *)sender {
}


    

     //[self presentViewController:_mailComposer animated:YES];
//    UIAlertView* alerta = [[UIAlertView alloc]initWithTitle:@"Pronto te contactaremos" message:@"Nuestro equipo de atención pronto te contactará para resolver todas tus dudas." delegate:nil cancelButtonTitle:@"Ok, Gracias HP" otherButtonTitles:nil, nil];
//    [alerta show];

- (IBAction)ayuda:(UIBarButtonItem *)sender {
    _mailComposer = [[MFMailComposeViewController alloc]init];
    _mailComposer.mailComposeDelegate = self;
    
        [_mailComposer.navigationController.navigationBar setBackgroundColor:[UIColor redColor]];
        [_mailComposer.navigationBar setBackgroundColor:[UIColor redColor]];

    NSArray* destinatarios = [[NSArray alloc]initWithObjects:@"rayulian@gmail.com", nil];
    [_mailComposer setToRecipients:destinatarios];
    [_mailComposer setSubject:@"Ayuda HP: Agenda"];
    [_mailComposer setMessageBody:@"Favor de contactarme con los siguientes datos: \nNombre: \nTelefono Personal: \nTelefono de casa: \nMail:" isHTML:NO];
    
    
    [self presentViewController:_mailComposer animated:YES completion:nil];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    
    [controller.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        [controller.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self dismissModalViewControllerAnimated:YES];
    
}
@end
