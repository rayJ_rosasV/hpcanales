//
//  EventosTableViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 06/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "EventosTableViewController.h"
#import "DetalleEventosViewController.h"

@interface EventosTableViewController ()

@end

@implementation EventosTableViewController{
NSMutableArray *promos;
NSMutableArray *promosNombres;

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    
    self.tabla.delegate=self;
    self.tabla.dataSource=self;
    _tabla.contentInset = UIEdgeInsetsMake(20.0,0.0,0.0,0.0);

    
    [NSThread detachNewThreadSelector:@selector(obtenerJson) toTarget:self withObject:nil];
    //NSLog(@"arreglo editado %@",promos);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Json

-(void) obtenerJson{
    
    NSError *error;
    NSDictionary *dict= [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://hp-canales.com.mx/hp_json.php?c=eventos"]] options:NSJSONReadingMutableContainers error:&error];
    promos =[dict objectForKey:@"Entradas"];
    //NSLog(@"EntradasE: %@",dict);
    
    //NSLog(@"promosE: %@",promos);
    
    
    
    [self.tabla reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!promos) {
        //NSLog(@"El arreglo esta vacio");
    }
    return [promos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cadena= @"celda";
    UITableViewCell *celda= [tableView dequeueReusableCellWithIdentifier:cadena];
    
    if (!celda) {
        celda= [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cadena];
    }
    
    if (promos) {
        if (indexPath.row < [promos count]) {
            celda.textLabel.text=[[promos objectAtIndex:indexPath.row] objectForKey:@"titulo"];
        }
    }
    
    
    return celda;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    DetalleEventosViewController* destinationViewController = segue.destinationViewController;
            destinationViewController.dirPagina = [[promos objectAtIndex:indexPath.row] objectForKey:@"id"];
}

@end
