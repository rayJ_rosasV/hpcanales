//
//  mail.h
//  hpcanales
//
//  Created by Raymundo Julian on 08/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface mail : NSObject <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) MFMailComposeViewController *mailComposer;

-(void) AgregarContenido: (NSString*) seccion;

@end
