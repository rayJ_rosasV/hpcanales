//
//  DetalleGeneralPromosViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 03/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "DetalleGeneralPromosViewController.h"

@interface DetalleGeneralPromosViewController ()

@end

@implementation DetalleGeneralPromosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
//    NSString* jQuey=[[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"html" encoding:NSUTF8StringEncoding error:nil];
    
    NSURL* jqueryURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"jquery" ofType:@"html"]];
    NSString* jqueryContent = [NSString stringWithContentsOfURL:jqueryURL encoding:NSUTF8StringEncoding error:nil];
    
    NSURL* siteURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"dir" ofType:@"html"]];
    NSString* dirContent = [NSString stringWithContentsOfURL:siteURL encoding:NSUTF8StringEncoding error:nil];
    

    NSMutableString* direccionHTML= [NSMutableString stringWithString:dirContent];
    //NSLog(@"dirpag= %@",_dirPagina);
    
    [direccionHTML appendString:_dirPagina];
    //NSLog(@"dirnuevapag= %@",direccionHTML);

    NSURL *url = [[NSURL alloc] initWithString:direccionHTML];
    
     NSMutableString* contenidoHTML= [NSMutableString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    [contenidoHTML appendString:jqueryContent];

    [_webView loadHTMLString:contenidoHTML baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)ayudaHP:(UIButton *)sender {
    
    UIAlertView* alerta = [[UIAlertView alloc]initWithTitle:@"Pronto te contactaremos" message:@"Nuestro equipo de atención pronto te contactará para resolver todas tus dudas." delegate:nil cancelButtonTitle:@"Ok, Gracias HP" otherButtonTitles:nil, nil];
    [alerta show];
}
@end
