//
//  promosCell.h
//  hpcanales
//
//  Created by Raymundo Julian on 31/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface promosCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UITextView *texto;
@property (weak, nonatomic) IBOutlet UIImageView *imagen;

@end
