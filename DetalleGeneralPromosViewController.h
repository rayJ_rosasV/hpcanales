//
//  DetalleGeneralPromosViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 03/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetalleGeneralPromosViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString* dirPagina;

- (IBAction)ayudaHP:(UIButton *)sender;
@end
