//
//  MapsViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 10/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "MapsViewController.h"


@interface MapsViewController ()

@end

@implementation MapsViewController{
    BOOL locActiv;
    CLLocationCoordinate2D coordenadas;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view, typically from a nib.
    self.vistaMapa.delegate=self;

    
    NSLog(@"mkcoordinate: %@",[self.vistaMapa userLocation]);
    //CLLocationCoordinate2D loc=[self.vistaMapa userLocation].coordinate;
    self.label.text = @"Centro de Convenciones Banamex";
    
    MKPointAnnotation *locpin=[[MKPointAnnotation alloc] init];
    coordenadas.latitude = 19.4417;
    coordenadas.longitude = -99.2234;
    //19.4417,-99.2234
    
    [locpin setCoordinate:coordenadas];
    [locpin setTitle:@"Titulo"];
    [locpin setSubtitle: @"MySubPin"];
    
    [self.vistaMapa addAnnotation:locpin];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)comoLlegar:(UIButton *)sender {
    
    //create MKMapItem out of coordinates
    MKPlacemark* placeMark = [[MKPlacemark alloc] initWithCoordinate: coordenadas addressDictionary:nil];
    MKMapItem* destination =  [[MKMapItem alloc] initWithPlacemark:placeMark];
    

    [destination openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving}];

}

- (IBAction)botonmapa:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.vistaMapa setMapType:MKMapTypeStandard];
            break;
        case 1:
            [self.vistaMapa setMapType:MKMapTypeSatellite];
            break;
        case 2:
            [self.vistaMapa setMapType:MKMapTypeHybrid];
            break;
            
        default:
            break;
    }
}

- (IBAction)localizacion:(UISwitch *)sender {
    
    (_botonloc.on) ? (self.vistaMapa.showsUserLocation = YES) : (self.vistaMapa.showsUserLocation = NO);
}

- (IBAction)Atras:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}




@end
