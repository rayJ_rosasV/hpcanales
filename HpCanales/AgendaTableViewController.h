//
//  AgendaTableViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 05/02/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITableView *tabla;

@end
