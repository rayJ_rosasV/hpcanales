//
//  DataManager.h
//  HpCanales
//
//  Created by Raymundo Julian on 09/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

@property NSString* codigo;

-(void) guardarDatosPromos: (NSMutableArray*) arreglo;
-(NSMutableArray*) cargarDatosPromos;

-(void) guardarDatos: (NSString*) codigo conNombre: (NSString*) nombre;
-(NSString*) obtenerPath;
-(void) cargarDatos: (NSString*) nombre;
-(NSString*) obtenerPath: (NSString*) nombre;
@end
