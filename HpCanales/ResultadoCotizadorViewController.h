//
//  ResultadoCotizadorViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 26/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultadoCotizadorViewController : UIViewController <UITabBarDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray* seleccion;
@property (strong, nonatomic) NSMutableArray* subtitulos;
@property (strong, nonatomic) NSMutableArray* tintaLaser;
@property (strong, nonatomic) NSMutableArray* numUsers;
@property (strong, nonatomic) NSMutableArray* multiImp;


@property (weak, nonatomic) IBOutlet UITableView *tabla;
- (IBAction)ayudaHP:(UIButton *)sender;

@end
