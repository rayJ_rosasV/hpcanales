//
//  ResultadosComputoViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 03/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "ResultadosComputoViewController.h"

@interface ResultadosComputoViewController ()

@end

@implementation ResultadosComputoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)atras:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
