//
//  collectionViewCell.h
//  HpCanales
//
//  Created by Raymundo Julian on 17/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface collectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *texto;

@end
