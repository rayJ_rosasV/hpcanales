//
//  FAQsViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 08/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "FAQsViewController.h"

@interface FAQsViewController ()

@end

@implementation FAQsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *url = [[NSURL alloc] initWithString:@"http://hp-canales.com.mx/directorio_mayoristas-2/"];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ayudaHP:(UIButton *)sender {
    UIAlertView* alerta = [[UIAlertView alloc]initWithTitle:@"Pronto te contactaremos" message:@"Nuestro equipo de atención pronto te contactará para resolver todas tus dudas." delegate:nil cancelButtonTitle:@"Ok, Gracias HP" otherButtonTitles:nil, nil];
    [alerta show];
}


- (IBAction)refresh:(UIBarButtonItem *)sender {
    [_webView reload];
}

- (IBAction)adelante:(UIBarButtonItem *)sender {
    [_webView goForward];
}

- (IBAction)atras:(UIBarButtonItem *)sender {
    [_webView goBack];
}

- (IBAction)stop:(UIBarButtonItem *)sender {
    [_webView stopLoading];
}
@end
