//
//  DetalleNotViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 26/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "DetalleNotViewController.h"

@interface DetalleNotViewController ()

@end

@implementation DetalleNotViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    NSURL* jqueryURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"jquery" ofType:@"html"]];
    NSString* jqueryContent = [NSString stringWithContentsOfURL:jqueryURL encoding:NSUTF8StringEncoding error:nil];
    
    NSURL* siteURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"dir" ofType:@"html"]];
    NSString* dirContent = [NSString stringWithContentsOfURL:siteURL encoding:NSUTF8StringEncoding error:nil];
    
    
    NSMutableString* direccionHTML= [NSMutableString stringWithString:dirContent];
    //NSLog(@"dirpag= %@",_dirPagina);
    
    [direccionHTML appendString:_dirPagina];
    //NSLog(@"dirnuevapag= %@",direccionHTML);
    
    NSURL *url = [[NSURL alloc] initWithString:direccionHTML];
    
    NSMutableString* contenidoHTML= [NSMutableString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    [contenidoHTML appendString:jqueryContent];
    
    [_webView loadHTMLString:contenidoHTML baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)atras:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
