//
//  DetalleNotasViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 26/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "DetalleNotasViewController.h"

@interface DetalleNotasViewController ()

@end

@implementation DetalleNotasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
