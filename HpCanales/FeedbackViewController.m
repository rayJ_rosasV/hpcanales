//
//  FeedbackViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 09/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    [[self.campoTexto layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.campoTexto layer] setBorderWidth:2.3];
    [[self.campoTexto layer] setCornerRadius:15];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)atras:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)enviar:(UIButton *)sender {
    [_campoTexto resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)cancelar:(UIButton *)sender {
    [_campoTexto resignFirstResponder];
    [_campoTexto setText:@""];
    
}
@end
