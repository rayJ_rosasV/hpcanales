//
//  AppDelegate.h
//  HpCanales
//
//  Created by Raymundo Julian on 29/11/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
