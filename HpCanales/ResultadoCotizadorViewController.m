//
//  ResultadoCotizadorViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 26/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "ResultadoCotizadorViewController.h"
#import "celdaResultadosCell.h"

@interface ResultadoCotizadorViewController ()

@end

@implementation ResultadoCotizadorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.tabla.delegate=self;
    self.tabla.dataSource=self;
    NSLog(@"Seleccion: %@",_seleccion);
    NSLog(@"Seleccion2: %@",_subtitulos);
    NSLog(@"MultiImo: %@",_multiImp);



    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cadena= @"celda";
    celdaResultadosCell *celda= [tableView dequeueReusableCellWithIdentifier:cadena];
    
    if (!celda) {
        celda= [[celdaResultadosCell alloc]init];
    }
    
    celda.titulo.text=[_seleccion objectAtIndex:indexPath.row];
//    celda.textLabel.text= @"Hola k ase?";
    //celda.detailTextLabel.text = [_subtitulos objectAtIndex:indexPath.row];
    celda.imagen.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", [_subtitulos objectAtIndex:indexPath.row]]];
    if ([[_tintaLaser objectAtIndex:indexPath.row] isEqualToString:@""]) {
        [[_tintaLaser objectAtIndex:indexPath.row] stringByAppendingString: @"N/A"];
    }

    celda.numUsuers.text = [_numUsers objectAtIndex:indexPath.row];

        
    
    NSLog(@"MultiImp: %@",[_multiImp objectAtIndex:indexPath.row]);
    celda.multiImp.text = [_multiImp objectAtIndex:indexPath.row];
    celda.tintalaser.text = [_tintaLaser objectAtIndex:indexPath.row];
    return celda;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [_seleccion count];
}
- (IBAction)ayudaHP:(UIButton *)sender {
    UIAlertView* alerta = [[UIAlertView alloc]initWithTitle:@"Pronto te contactaremos" message:@"Nuestro equipo de atención pronto te contactará para resolver todas tus dudas." delegate:nil cancelButtonTitle:@"Ok, Gracias HP" otherButtonTitles:nil, nil];
    [alerta show];
}
@end
