//
//  FAQsViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 08/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textoFAQs;
- (IBAction)ayudaHP:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)refresh:(UIBarButtonItem *)sender;
- (IBAction)adelante:(UIBarButtonItem *)sender;
- (IBAction)atras:(UIBarButtonItem *)sender;
- (IBAction)stop:(UIBarButtonItem *)sender;

@end
