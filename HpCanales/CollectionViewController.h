//
//  CollectionViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 17/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *coleccion;

@end
