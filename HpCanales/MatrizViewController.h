//
//  MatrizViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 12/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatrizViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
- (IBAction)enviar:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrolView;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerColor;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerImpresoraMulti;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerNumUsuarios;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerLaserTinta;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerTamanoPapel;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerVolImprMin;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerVolImpMax;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerVelocidad;
@property (weak, nonatomic) IBOutlet UIImageView *cancelar;
@property (weak, nonatomic) IBOutlet UITextField *tintaT;
@property (weak, nonatomic) IBOutlet UITextField *impMultiT;
@property (weak, nonatomic) IBOutlet UITextField *numUsersT;
@property (weak, nonatomic) IBOutlet UITextField *LaserTintT;
@property (weak, nonatomic) IBOutlet UITextField *tamanoPapelT;
@property (weak, nonatomic) IBOutlet UITextField *volminT;
@property (weak, nonatomic) IBOutlet UITextField *volmaxT;
@property (weak, nonatomic) IBOutlet UITextField *velT;


@end
