//
//  DataManager.m
//  HpCanales
//
//  Created by Raymundo Julian on 09/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager


-(void) guardarDatosPromos: (NSMutableArray*) arreglo{
    NSArray* valor = [[NSArray alloc]initWithObjects:arreglo, nil];
    [valor writeToFile:[self obtenerPath:@"ArregloPromos.plist"] atomically:YES];
}
-(NSMutableArray*) cargarDatosPromos{
    NSString* objetoPath = [self obtenerPath:@"ArregloPromos.plist"];
    BOOL existeArchivo = [[NSFileManager defaultManager] fileExistsAtPath:objetoPath];
    if (existeArchivo) {
        NSMutableArray* valores = [[NSMutableArray alloc]initWithContentsOfFile:objetoPath];
        return valores;
    }
    else{
        return NULL;
    }
    
}




-(void) guardarDatos: (NSString*) codigo conNombre: (NSString*) nombre{
    NSArray* valor = [[NSArray alloc]initWithObjects:codigo, nil];
    [valor writeToFile:[self obtenerPath:nombre] atomically:YES];
}
-(void) cargarDatos: (NSString*) nombre{
    NSString* objetoPath = [self obtenerPath:nombre];
    BOOL existeArchivo = [[NSFileManager defaultManager] fileExistsAtPath:objetoPath];
    if (existeArchivo) {
        NSArray* valores = [[NSArray alloc]initWithContentsOfFile:objetoPath];
        _codigo = [valores objectAtIndex:0];
    }
    
    
}
-(NSString*) obtenerPath: (NSString*) nombre{
    NSArray* pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[pathArray objectAtIndex:0] stringByAppendingString:nombre];
}
-(NSString*) obtenerPath{
    NSArray* pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[pathArray objectAtIndex:0] stringByAppendingString:@"guardado.plist"];
}



@end
