//
//  PromocionesTableViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 29/11/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface PromocionesTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (weak, nonatomic) IBOutlet UINavigationItem *navbar;
- (IBAction)menuBoton:(UIBarButtonItem *)sender;

@end
