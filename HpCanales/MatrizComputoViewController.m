//
//  MatrizComputoViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 03/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "MatrizComputoViewController.h"
#import "CHCSVParser.h"
#import "ResultadosComputoViewController.h"

@interface Delegate2 : NSObject <CHCSVParserDelegate>
@property (readonly) NSArray *lines;

@end

@interface MatrizComputoViewController (){
    
    NSArray* producto;
    NSArray* familia;
    NSArray* chasis;
    NSArray* pantalla;
    NSArray* procesador;
    NSArray* generacionModelo;
    NSArray* rAM;
    NSArray* discoDuro;
    NSArray* sO;
    NSString* sku;

    
    NSMutableArray* prueba;
    NSString* resultados;
    NSMutableDictionary* contenido;
}

@end
@implementation Delegate2 {
    NSMutableArray *_lines;
    NSMutableArray *_currentLine;
}
- (void)dealloc {

}
- (void)parserDidBeginDocument:(CHCSVParser *)parser {
    _lines = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber {
    _currentLine = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex {
    //NSLog(@"%@", field);
    [_currentLine addObject:field];
}
- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber {
    [_lines addObject:_currentLine];
    _currentLine = nil;
}
- (void)parserDidEndDocument:(CHCSVParser *)parser {
    //	NSLog(@"parser ended: %@", csvFile);
}
- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
	NSLog(@"ERROR: %@", error);
    _lines = nil;
}
@end

@implementation MatrizComputoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //_scrolView.contentSize = CGSizeMake(320, 1980);
    _pickerProducto.delegate = self;
    _pickerFamilia.delegate = self;
    _pickerChasis.delegate = self;
    _pickerPantalla.delegate = self;
    _pickerProcesador.delegate = self;
    _pickerGeneracionModelo.delegate = self;
    _pickerRAM.delegate = self;
    _pickerDiscoDuro.delegate = self;
    _pickerSO.delegate = self;
    
    contenido = [[NSMutableDictionary alloc]init];
    NSString* nombreFormato = [[NSString alloc]init];
    //    opciones= [[NSMutableArray alloc]init];
    //resultados = [[NSString alloc]init];
    int i =0 ;
    //
    NSString *file = @(__FILE__);
    file = [[file stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Computo.csv"];
	
	NSLog(@"Beginning...");
	NSStringEncoding encoding = 0;
    NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:file];
	CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:';'];
    [p setRecognizesBackslashesAsEscapes:YES];
    [p setSanitizesFields:YES];
	
	NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
	
	Delegate2 * d2 = [[Delegate2 alloc] init];
	[p setDelegate:d2];
	
	NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
	[p parse];
	NSTimeInterval end = [NSDate timeIntervalSinceReferenceDate];
	
	NSLog(@"raw difference: %f", (end-start));
    
    NSLog(@"d lines: %@",[d2 lines]);

    NSLog(@"L1: %@",[[d2 lines]objectAtIndex:2]);
    id cadena1= [[[d2 lines]objectAtIndex:2] componentsJoinedByString:@","];
	NSLog(@"Array: %@",cadena1);
    NSString* hola = [[NSString alloc]init];
    hola = cadena1;
	NSLog(@"Cadnea: %@",hola);
    NSArray* final = [[NSMutableArray alloc]init];
    final = [hola componentsSeparatedByString:@","];
    NSLog(@"final: %@ y %@",final,[final objectAtIndex:4]);
	
    for (NSArray* line in [d2 lines]) {
        cadena1= [line componentsJoinedByString:@","];
        NSLog(@"Array: %@",cadena1);
        hola = cadena1;
        NSLog(@"Cadnea: %@",hola);
        final = [hola componentsSeparatedByString:@","];
        NSLog(@"final: %@ y %@",final,[final objectAtIndex:3]);
        nombreFormato = [NSString stringWithFormat:@"Computo%d",i];
        [contenido setObject:final forKey:nombreFormato];
        i++;
        
    }
    
    producto = [[NSArray alloc]initWithObjects:@"Indistinto",@"All In One",@"Desktops",@"ElItePad",@"Notebooks",@"WorkStatIons", nil];
    familia=[[NSArray alloc]initWithObjects:@"Indistinto",@"205",@"240",@"245",@"250",@"400",@"405",@"440",@"450",@"600",@"640",@"800",@"840",@"900",@"1040",@"3500",@"4300",@"6305",@"9470m",@"Z1",@"Z220",@"Z420",@"Z620",@"Z820", nil];
    chasis=[[NSArray alloc]initWithObjects:@"Indistinto",@"AiO",@"MT",@"RMT",@"SFF",@"N/A", nil];
    pantalla=[[NSArray alloc]initWithObjects:@"Indistinto",@"10.1\"",@"14\"",@"15.6\"",@"18.5\"",@"20\"",@"21.5\"",@"23\"",@"27\"",@"N/A", nil];
    procesador=[[NSArray alloc]initWithObjects:@"Indistinto",@"AMD ",@"Atom ",@"CELERON ",@"CORE I3 ",@"Core I5 ",@"Core I7 ",@"Dual Core ",@"PentIum ",@"QUAD CORE ",@"XEON ", nil];
    generacionModelo=[[NSArray alloc]initWithObjects:@"Indistinto",@" A8-6500B",@" E3 1225V2",@" E5-1603 2.810MB",@"E5-16503.212MB",@"E5-26402.5GHZ",@"1000M",@"2a",@"3a",@"4a",@"4a vPro",@"A4-5000  ",@"E1-2100",@"E1-2500",@"E31225V2",@"E31245V2",@"E3 1270 3.5",@"E3-1225v2 3.3",@"E3-1240v2 3.4",@"E5-1607 3 10M",@"E5-1620 3.6 10MB",@"E5-1650 3.2 12MB",@"E5-2609 2.4 GHZ",@"E5-2620",@"5/3.5", nil];
    rAM=[[NSArray alloc]initWithObjects:@"Indistinto",@"4GB",@"16GB",@"2GB",@"8 GB  ", nil];
    discoDuro=[[NSArray alloc]initWithObjects:@"Indistinto",@"1 TB",@"2 TB",@"256 GB",@"32 GB",@"320 GB",@"500 GB",@"750 GB", nil];
    sO=[[NSArray alloc]initWithObjects:@"Indistinto",@"WIN 8.1 ",@"WIN 8   ",@"WIN 7",@"Linux ",@"FreeDOS", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView==_pickerProducto) {
        return [producto count];
        
    }
    else if (pickerView==_pickerFamilia){
        return [familia count];
        
    }
    else if (pickerView==_pickerChasis){
        return [chasis count];
        
        
    }
    else if (pickerView==_pickerPantalla){
        return [pantalla count];
        
        
    }
    else if (pickerView==_pickerProcesador){
        return [procesador count];

        
    }
    else if (pickerView==_pickerGeneracionModelo){
        return [generacionModelo count];
        
        
    }
    else if (pickerView==_pickerRAM){
        return [rAM count];
        
        
    }
    else if (pickerView==_pickerDiscoDuro){
        return [discoDuro count];
        
        
    }
    else if (pickerView==_pickerSO){
        return [sO count];
        
        
    }
        else{
        return 0;
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView==_pickerProducto) {
        return [producto objectAtIndex:row];
        
    }
    else if (pickerView==_pickerFamilia){
        return [familia objectAtIndex:row];
        
    }
    else if (pickerView==_pickerChasis){
        return [chasis objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerPantalla){
        return [pantalla objectAtIndex:row];
        

    }
    else if (pickerView==_pickerProcesador){
        return [procesador objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerGeneracionModelo){
        return [generacionModelo objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerRAM){
        return [rAM objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerDiscoDuro){
        return [discoDuro objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerSO){
        return [sO objectAtIndex:row];
        
        
    }
    
    else{
        return @"error";
    }
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ResultadosComputoViewController* nuevaVista = segue.destinationViewController;
    
    
    //    [opciones addObject:[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]];
    //    [opciones addObject:[numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]]];
    //    [opciones addObject:[laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]]];
    //    [opciones addObject:[TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]]];
    //    [opciones addObject:[volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]]];
    //    [opciones addObject:[volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]]];
    //    [opciones addObject:[velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]]];
    //
    //  NSLog(@"opciones: %@",opciones);
    nuevaVista.seleccion = [[NSMutableArray alloc]init];
    nuevaVista.subtitulos = [[NSMutableArray alloc]init];
    [nuevaVista.seleccion removeAllObjects];
    [nuevaVista.subtitulos removeAllObjects];
    //    nuevaVista.seleccion = [NSArray arrayWithArray:opciones];
    //    [nuevaVista.seleccion addObject:[color objectAtIndex:[_pickerColor selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]]];
    //[nuevaVista.seleccion arrayByAddingObjectsFromArray:opciones];
    
    for (NSString* elemento in contenido) {
        if ([[[contenido objectForKey:elemento]objectAtIndex:0] isEqualToString: [producto objectAtIndex:[_pickerProducto selectedRowInComponent:0]]]|| [[producto objectAtIndex:[_pickerProducto selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
            NSLog(@"producto");
            
            if ([[[contenido objectForKey:elemento]objectAtIndex:1] isEqualToString: [familia objectAtIndex:[_pickerFamilia selectedRowInComponent:0]]]|| [[familia objectAtIndex:[_pickerFamilia selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                NSLog(@"familia");
                
                
                if ([[[contenido objectForKey:elemento]objectAtIndex:2] isEqualToString: [chasis objectAtIndex:[_pickerChasis selectedRowInComponent:0]]]|| [[chasis objectAtIndex:[_pickerChasis selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                    NSLog(@"chasis");
                    
                    
                    if ([[[contenido objectForKey:elemento]objectAtIndex:3] isEqualToString: [pantalla objectAtIndex:[_pickerPantalla selectedRowInComponent:0]]] || [[pantalla objectAtIndex:[_pickerPantalla selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                        NSLog(@"lasertinta");
                        
                        if ([[[contenido objectForKey:elemento]objectAtIndex:6] isEqualToString: [procesador objectAtIndex:[_pickerProcesador selectedRowInComponent:0]]] || [[procesador objectAtIndex:[_pickerProcesador selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                            NSLog(@"tamaño papel");
                            
                            if ([[[contenido objectForKey:elemento]objectAtIndex:7] isEqualToString: [generacionModelo objectAtIndex:[_pickerGeneracionModelo selectedRowInComponent:0]]] || [[generacionModelo objectAtIndex:[_pickerGeneracionModelo selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                NSLog(@"colimpmin");
                                
                                if ([[[contenido objectForKey:elemento]objectAtIndex:8] isEqualToString: [rAM objectAtIndex:[_pickerRAM selectedRowInComponent:0]]] || [[rAM objectAtIndex:[_pickerRAM selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                    NSLog(@"volimpmax");
                                    
                                    if ([[[contenido objectForKey:elemento]objectAtIndex:9] isEqualToString: [discoDuro objectAtIndex:[_pickerDiscoDuro selectedRowInComponent:0]]] || [[discoDuro objectAtIndex:[_pickerDiscoDuro selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                        NSLog(@"velocidad");
                                        
                                        if ([[[contenido objectForKey:elemento]objectAtIndex:9] isEqualToString: [sO objectAtIndex:[_pickerSO selectedRowInComponent:0]]] || [[sO objectAtIndex:[_pickerSO selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                            NSLog(@"velocidad");
                                        
                                        resultados = [NSString stringWithFormat:@"%@",[[contenido objectForKey:elemento]objectAtIndex:11]];
                                        sku = [NSString stringWithFormat:@"%@",[[contenido objectForKey:elemento]objectAtIndex:10]];
                                        NSLog(@"Sip: %@",resultados);
                                        NSLog(@"Sipp: %@",sku);
                                        
                                        [nuevaVista.seleccion addObject:resultados];
                                        [nuevaVista.subtitulos addObject:sku];
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
        }
        
        //        NSLog(@"elemento: %@",elemento);
        //        NSLog(@"contenido: %@", [[contenido objectForKey:elemento]objectAtIndex:1]);
        //        NSLog(@"picker: %@",[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]);
    }
}
@end
