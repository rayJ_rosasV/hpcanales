//
//  MatrizViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 12/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "MatrizViewController.h"
#import "CHCSVParser.h"
#import "ResultadoCotizadorViewController.h"

@interface Delegate : NSObject <CHCSVParserDelegate>

@property (readonly) NSArray *lines;

@end

@interface MatrizViewController (){
    NSArray* color;
    NSArray* impresoraMulti;
    NSArray* laserTinta;
    NSArray* numUsuarios;
    NSArray* TamanoPapel;
    NSArray* velocidad;
    NSArray* volImpMax;
    NSArray* volImpMin;
    NSMutableArray* prueba;
    NSString* resultados;
    NSString* sku;
    NSString* numeroDeUsuarios;
    NSString* multifuncionalOImpresora;
    NSString* tintaOLaser;
    
    NSMutableDictionary* contenido;
    
    NSDictionary* problema1;
    
    NSArray *argsArray;
    
    //    NSMutableArray* opciones;
    
    
}

@end


@implementation Delegate {
    NSMutableArray *_lines;
    NSMutableArray *_currentLine;
}
- (void)dealloc {
    
}
- (void)parserDidBeginDocument:(CHCSVParser *)parser {
    _lines = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber {
    _currentLine = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex {
    //NSLog(@"%@", field);
    [_currentLine addObject:field];
    
}
- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber {
    [_lines addObject:_currentLine];
    _currentLine = nil;
}
- (void)parserDidEndDocument:(CHCSVParser *)parser {
    //	NSLog(@"parser ended: %@", csvFile);
}
- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
	NSLog(@"ERROR: %@", error);
    _lines = nil;
}
@end
@implementation MatrizViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    problema1= [[NSDictionary alloc]init];
    _scrolView.contentSize = CGSizeMake(320, 1980);
    
    _pickerColor = [[UIPickerView alloc] initWithFrame:CGRectZero];
    _pickerColor.delegate=self;
    _pickerColor.dataSource=self;
    [_pickerColor setShowsSelectionIndicator:YES];
    _tintaT.inputView = _pickerColor;
    
    _pickerImpresoraMulti = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerImpresoraMulti.delegate = self;
    _pickerImpresoraMulti.dataSource = self;
    [_pickerImpresoraMulti setShowsSelectionIndicator:YES];
    _impMultiT.inputView = _pickerImpresoraMulti;
    
    _pickerLaserTinta = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerLaserTinta.delegate=self;
    _pickerLaserTinta.dataSource = self;
    [_pickerLaserTinta setShowsSelectionIndicator:YES];
    _LaserTintT.inputView = _pickerLaserTinta;
    
    _pickerNumUsuarios = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerNumUsuarios.dataSource = self;
    _pickerNumUsuarios.delegate=self;
    [_pickerNumUsuarios setShowsSelectionIndicator:YES];
    _numUsersT.inputView = _pickerNumUsuarios;
    
    _pickerTamanoPapel = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerTamanoPapel.dataSource = self;
    _pickerTamanoPapel.delegate=self;
    [_pickerTamanoPapel setShowsSelectionIndicator:YES];
    _tamanoPapelT.inputView = _pickerTamanoPapel;
    
    _pickerVelocidad = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerVelocidad.dataSource = self;
    _pickerVelocidad.delegate=self;
    [_pickerVelocidad setShowsSelectionIndicator:YES];
    _velT.inputView = _pickerVelocidad;
    
    _pickerVolImpMax = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerVolImpMax.dataSource = self;
    _pickerVolImpMax.delegate=self;
    [_pickerVolImpMax setShowsSelectionIndicator:YES];
    _volmaxT.inputView = _pickerVolImpMax;
    
    _pickerVolImprMin = [[UIPickerView alloc]initWithFrame:CGRectZero];
    _pickerVolImprMin.dataSource = self;
    _pickerVolImprMin.delegate=self;
    [_pickerVolImprMin setShowsSelectionIndicator:YES];
    _volminT.inputView = _pickerVolImprMin;
    
    contenido = [[NSMutableDictionary alloc]init];
    //    opciones= [[NSMutableArray alloc]init];
    resultados = [[NSString alloc]init];
    sku = [[NSString alloc]init];
    
    //
    //    int i =0 ;
    ////
    //    NSString *file = @(__FILE__);
    //    file = [[file stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Impresion.csv"];
    //
    //
    //	NSLog(@"Beginning...");
    //	NSStringEncoding encoding = 0;
    //    NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:file];
    //	CHCSVParser * p = [[CHCSVParser alloc] initWithInputStream:stream usedEncoding:&encoding delimiter:';'];
    //    [p setRecognizesBackslashesAsEscapes:YES];
    //    [p setSanitizesFields:YES];
    //
    //	NSLog(@"encoding: %@", CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(encoding)));
    //
    //	Delegate * d = [[Delegate alloc] init];
    //	[p setDelegate:d];
    //	NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    //	[p parse];
    //	NSTimeInterval end = [NSDate timeIntervalSinceReferenceDate];
    //
    //	NSLog(@"raw difference: %f", (end-start));
    //
    //    NSLog(@"D lines: %@", [d lines]);
    //
    //
    //
    //NSLog(@"L1: %@",[[d lines]objectAtIndex:2] );
    //    id cadena1= [[[d lines]objectAtIndex:2] componentsJoinedByString:@","];
    //	NSLog(@"Array: %@",cadena1);
    //    NSString* hola = [[NSString alloc]init];
    //    hola = cadena1;
    //	NSLog(@"Cadnea: %@",hola);
    //    NSArray* final = [[NSMutableArray alloc]init];
    //    final = [hola componentsSeparatedByString:@","];
    //    NSLog(@"final: %@ y %@",final,[final objectAtIndex:4]);
    //
    //    NSString *className = NSStringFromClass([[d lines] class]);
    //    NSLog(@"Clase: %@", className);
    ////
    ////
    //    for (NSArray* line in [d lines]) {
    //        cadena1= [line componentsJoinedByString:@","];
    //        NSLog(@"Array: %@",cadena1);
    //        hola = cadena1;
    //        NSLog(@"Cadnea: %@",hola);
    //        final = [hola componentsSeparatedByString:@","];
    //        NSLog(@"final: %@ y %@",final,[final objectAtIndex:3]);
    //        nombreFormato = [NSString stringWithFormat:@"Impresion%d",i];
    //        [contenido setObject:final forKey:nombreFormato];
    //        i++;
    //
    //    }
    //    NSLog(@"valor: %@",[contenido objectForKey:@"Impresion5"]);
    //
    //    NSLog(@"valor2: %@",[[contenido objectForKey:@"Impresion5"] objectAtIndex:3]);
    
    
    //    NSLog(@"sep: %@",listItems[0]);
    
    
    // Get contents oj JSON
    
    [self obtenerJson];
    
    //
    color = [[NSArray alloc]initWithObjects:@"Indistinto",@"Color",@"Monocromàtico", nil];
    impresoraMulti=[[NSArray alloc]initWithObjects:@"Indistinto",@"Impresora",@"Multifuncional", nil];
    laserTinta=[[NSArray alloc]initWithObjects:@"Indistinto",@"Làser",@"Tinta", nil];
    numUsuarios=[[NSArray alloc]initWithObjects:@"Indistinto",@"1 a 3",@"3 a 5",@"3 a 6",@"6 a 10",@"8 a 15",@"10 a 15",@"10 a 20",@"15 a 20",@"20 a 40", nil];
    TamanoPapel=[[NSArray alloc]initWithObjects:@"Indistinto",@"Carta",@"Hasta Doble Carta", nil];
    velocidad=[[NSArray alloc]initWithObjects:@"Indistinto",@"14",@"17",@"19",@"21",@"26",@"30",@"31",@"33",@"35",@"40",@"42",@"45",@"50",@"52",@"55",@"62",@"18/13",@"20/15",@"20/16",@"21/17",@"23/22",@"32/29",@"33/29",@"5/3.5", nil];
    volImpMax=[[NSArray alloc]initWithObjects:@"Indistinto",@"400",@"500",@"600",@"800",@"850",@"1000",@"1500",@"2000",@"2500",@"3000",@"4000",@"5000",@"6000",@"7500",@"500",@"9000",@"1000",@"12000",@"15000",@"17000",@"20000",@"50000", nil];
    volImpMin=[[NSArray alloc]initWithObjects:@"Indistinto",@"100",@"150",@"200",@"250",@"300",@"500",@"750",@"1000",@"1500",@"2000",@"2500",@"3000",@"4000",@"5000",@"8000",@"15000", nil];
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView==_pickerColor) {
        return [color count];
        
    }
    else if (pickerView==_pickerImpresoraMulti){
        return [impresoraMulti count];
        
    }
    else if (pickerView==_pickerLaserTinta){
        return [laserTinta count];
        
        
    }
    else if (pickerView==_pickerNumUsuarios){
        return [numUsuarios count];
        
        
    }
    else if (pickerView==_pickerTamanoPapel){
        return [TamanoPapel count];
        
        
    }
    else if (pickerView==_pickerVelocidad){
        return [velocidad count];
        
        
    }
    else if (pickerView==_pickerVolImpMax){
        return [volImpMax count];
        
        
    }
    else if (pickerView==_pickerVolImprMin){
        return [volImpMin count];
        
        
    }
    else{
        return 0;
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView==_pickerColor) {
        return [color objectAtIndex:row];
        
    }
    else if (pickerView==_pickerImpresoraMulti){
        return [impresoraMulti objectAtIndex:row];
        
    }
    else if (pickerView==_pickerLaserTinta){
        return [laserTinta objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerNumUsuarios){
        return [numUsuarios objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerTamanoPapel){
        return [TamanoPapel objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerVelocidad){
        return [velocidad objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerVolImpMax){
        return [volImpMax objectAtIndex:row];
        
        
    }
    else if (pickerView==_pickerVolImprMin){
        return [volImpMin objectAtIndex:row];
        
        
    }
    else{
        return @"error";
    }
}


- (IBAction)enviar:(UIButton *)sender {
    
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ResultadoCotizadorViewController* nuevaVista = segue.destinationViewController;
    
    
    //    [opciones addObject:[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]];
    //    [opciones addObject:[numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]]];
    //    [opciones addObject:[laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]]];
    //    [opciones addObject:[TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]]];
    //    [opciones addObject:[volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]]];
    //    [opciones addObject:[volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]]];
    //    [opciones addObject:[velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]]];
    //
    //  NSLog(@"opciones: %@",opciones);
    nuevaVista.seleccion = [[NSMutableArray alloc]init];
    nuevaVista.subtitulos = [[NSMutableArray alloc]init];
    nuevaVista.numUsers = [[NSMutableArray alloc]init];
    nuevaVista.tintaLaser = [[NSMutableArray alloc]init];
    nuevaVista.multiImp = [[NSMutableArray alloc]init];
    [nuevaVista.seleccion removeAllObjects];
    [nuevaVista.subtitulos removeAllObjects];
    [nuevaVista.numUsers removeAllObjects];
    [nuevaVista.seleccion removeAllObjects];
    [nuevaVista.multiImp removeAllObjects];
    //    nuevaVista.seleccion = [NSArray arrayWithArray:opciones];
    //    [nuevaVista.seleccion addObject:[color objectAtIndex:[_pickerColor selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]]];
    //    [nuevaVista.seleccion addObject:[velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]]];
    //[nuevaVista.seleccion arrayByAddingObjectsFromArray:opciones];
    
    for (int i=0; i<[argsArray count]; i++) {
        problema1 = [argsArray objectAtIndex:i];
        if ([[problema1 objectForKey:@"Impresora o multifuncional"] isEqualToString: [impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]]|| [[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
            NSLog(@"impresoramulti");
            if ([[problema1 objectForKey:@"Monocromatica o  Color"] isEqualToString: [color objectAtIndex:[_pickerColor selectedRowInComponent:0]]]|| [[color objectAtIndex:[_pickerColor selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                NSLog(@"color");
                if ([[problema1 objectForKey:@"Numero de Usuarios"] isEqualToString: [numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]]]|| [[numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                    NSLog(@"numusers");
                    if ([[problema1 objectForKey:@"Laser o tinta"] isEqualToString: [laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]]] || [[laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                        NSLog(@"lasertinta");
                        if ([[problema1 objectForKey:@"Tamano de Papel"] isEqualToString: [TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]]] || [[TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                            NSLog(@"tamaño papel");
                            if ([[NSString stringWithFormat:@"%@",[problema1 objectForKey:@"Volumenmin"]]  isEqualToString: [volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]]] || [[volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                NSLog(@"volimpmin");
                                if ([[NSString stringWithFormat:@"%@",[problema1 objectForKey:@"Volumenmax"]] isEqualToString: [volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]]] || [[volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                    NSLog(@"volimpmax");
                                    if ([[problema1 objectForKey:@"Velocidad (ppm)"] isEqualToString: [velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]]] || [[velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]] isEqualToString:@"Indistinto"]) {
                                        NSLog(@"velocidad");
                                        resultados = [NSString stringWithFormat:@"%@",[problema1 objectForKey:@"Modelo"]];
                                        sku = [NSString stringWithFormat:@"%@",[problema1 objectForKey:@"SKU"]];
                                        numeroDeUsuarios = [NSString stringWithFormat:@"%@",[problema1 objectForKey:@"Numero de Usuarios"]];                                        tintaOLaser = [NSString stringWithFormat:@"%@",[problema1 objectForKey:@"Laser o Tinta"]];
                                        multifuncionalOImpresora = [NSString stringWithFormat:@"%@",[problema1 objectForKey:@"Impresora o multifuncional"]];
                                        
                                        NSLog(@"Sip: %@",resultados);
                                        NSLog(@"Sipp: %@",sku);
                                        [nuevaVista.seleccion addObject:resultados];
                                        [nuevaVista.subtitulos addObject:sku];
                                        [nuevaVista.numUsers addObject:numeroDeUsuarios];
                                        [nuevaVista.tintaLaser addObject:tintaOLaser];
                                        [nuevaVista.multiImp addObject:multifuncionalOImpresora];
                                        
                                        
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
            }
            
            
            
            
        }
        
        //    for (NSString* elemento in argsArray) {
        //
        //
        //
        //
        //
        //
        //
        //
        //
        //
        //
        //
        
        //
        //
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //
        //            }
        
    }
    
    //        NSLog(@"elemento: %@",elemento);
    //        NSLog(@"contenido: %@", [[contenido objectForKey:elemento]objectAtIndex:1]);
    //        NSLog(@"picker: %@",[impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]]);
    //}
}

-(void)dismissKeyboard {
    
    _tintaT.text = [color objectAtIndex:[_pickerColor selectedRowInComponent:0]];
    _impMultiT.text = [impresoraMulti objectAtIndex:[_pickerImpresoraMulti selectedRowInComponent:0]];
    _LaserTintT.text = [laserTinta objectAtIndex:[_pickerLaserTinta selectedRowInComponent:0]];
    _numUsersT.text = [numUsuarios objectAtIndex:[_pickerNumUsuarios selectedRowInComponent:0]];
    _velT.text = [velocidad objectAtIndex:[_pickerVelocidad selectedRowInComponent:0]];
    _tamanoPapelT.text = [TamanoPapel objectAtIndex:[_pickerTamanoPapel selectedRowInComponent:0]];
    _volmaxT.text = [volImpMax objectAtIndex:[_pickerVolImpMax selectedRowInComponent:0]];
    _volminT.text = [volImpMin objectAtIndex:[_pickerVolImprMin selectedRowInComponent:0]];
    
    [_tintaT resignFirstResponder];
    [_impMultiT resignFirstResponder];
    [_LaserTintT resignFirstResponder];
    [_numUsersT resignFirstResponder];
    [_velT resignFirstResponder];
    [_tamanoPapelT resignFirstResponder];
    [_volminT resignFirstResponder];
    [_volmaxT resignFirstResponder];
    
}

-(void) obtenerJson{
    
    NSError *error;
    NSDictionary *dict= [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"Impresion" withExtension:@"json"]] options:NSJSONReadingMutableContainers error:&error];
    argsArray = [[NSArray alloc] initWithArray:[dict objectForKey:@"args"]];
    
    NSLog(@"json impresion: %@",[argsArray objectAtIndex:0]);
    problema1 = [argsArray objectAtIndex:0];
    NSLog(@"Impresora: %@", [problema1 objectForKey:@"Impresora o multifuncional"]);
    NSLog(@"Laser o tinta: %@", [problema1 objectForKey:@"Laser o Tinta"]);
    NSLog(@"Modelo: %@", [problema1 objectForKey:@"Modelo"]);
    NSLog(@"Monocromatica o  Color: %@", [problema1 objectForKey:@"Monocromatica o  Color"]);
    NSLog(@"Numero de Usuarios: %@", [problema1 objectForKey:@"Numero de Usuarios"]);
    NSLog(@"SKU: %@", [problema1 objectForKey:@"SKU"]);
    NSLog(@"Tamano de Papel: %@", [problema1 objectForKey:@"Tamano de Papel"]);
    NSLog(@"Velocidad (ppm): %@", [problema1 objectForKey:@"Velocidad (ppm)"]);
    NSLog(@"Volumen de impresion por pagina maximo mensual recomendado: %@", [problema1 objectForKey:@"Volumenmax"]);
    NSLog(@"Volumen de impresion por pagina minimo mensual recomendado: %@", [problema1 objectForKey:@"Volumenmin"]);
    
    
}

@end
