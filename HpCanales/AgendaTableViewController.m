//
//  AgendaTableViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 05/02/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "AgendaTableViewController.h"
#import "celdaEventosCell.h"
#import "DetalleEventosViewController.h"

@interface AgendaTableViewController (){
    NSMutableArray *promos;

}

@end

@implementation AgendaTableViewController{
    NSArray* info1;
    NSArray* info2;
    NSArray* info3;
    NSArray* info4;
    NSArray* info5;
    NSArray* info6;
    NSArray* info7;
    NSArray* info8;
    NSArray* info9;
    NSArray* info10;
    NSArray* info11;
    NSArray* info12;
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabla.delegate = self;
    self.tabla.dataSource = self;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"5textura.jpg"]];
    [tempImageView setFrame:self.tableView.frame];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [self obtenerJson];

    
    info1 = [[NSArray alloc]initWithObjects:@"Sabado 25: Nuevas Impresoras",@"Lunes 23: Nuevas compus", nil];
    info1= promos;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 12;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger filas;
    
    switch (section)
    {
        case 0:
            filas =  [promos count];
            break;
        case 1:
            filas =  [info2 count];
            break;
        case 2:
            filas =  [info3 count];
            break;
        case 3:
            filas =  [info4 count];
            break;
        case 4:
            filas =  [info5 count];
            break;
        case 5:
            filas =  [info6 count];
            break;
        case 6:
            filas =  [info7 count];
            break;
        case 7:
            filas =  [info8 count];
            break;
        case 8:
            filas =  [info9 count];
            break;
        case 9:
            filas =  [info10 count];
            break;
        case 10:
            filas =  [info11 count];
            break;
        case 11:
            filas =  [info12 count];
            break;
            
            // ...
        default:
            filas =  0;
            break;
    }
    // Return the number of rows in the section.
    return filas;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"evento";
    celdaEventosCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
                cell= [[celdaEventosCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (promos) {
        if (indexPath.row < [promos count]) {
            cell.titulo.text=[[promos objectAtIndex:indexPath.row] objectForKey:@"titulo"];
            
        }
    }
    
    // Configure the cell...
    
    return cell;

}
-(void) obtenerJson{
    
    NSError *error;
    NSDictionary *dict= [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://hp-canales.com.mx/hp_json.php?c=eventos"]] options:NSJSONReadingMutableContainers error:&error];
    promos =[dict objectForKey:@"Entradas"];
    
    
    
    
    [self.tabla reloadData];
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = NSLocalizedString(@"Enero", @"Enero");
            break;
        case 1:
            sectionName = NSLocalizedString(@"Febrero", @"Febrero");
            break;
        case 2:
            sectionName = NSLocalizedString(@"Marzo", @"Marzo");
            break;
        case 3:
            sectionName = NSLocalizedString(@"Abril", @"Abril");
            break;
        case 4:
            sectionName = NSLocalizedString(@"Mayo", @"Mayo");
            break;
        case 5:
            sectionName = NSLocalizedString(@"Junio", @"Junio");
            break;
        case 6:
            sectionName = NSLocalizedString(@"Julio", @"Julio");
            break;
        case 7:
            sectionName = NSLocalizedString(@"Agosto", @"Agosto");
            break;
        case 8:
            sectionName = NSLocalizedString(@"Septiembre", @"Septiembre");
            break;
        case 9:
            sectionName = NSLocalizedString(@"Octubre", @"Octubre");
            break;
        case 10:
            sectionName = NSLocalizedString(@"Noviembre", @"Noviembre");
            break;
        case 11:
            sectionName = NSLocalizedString(@"Diciembre", @"Diciembre");
            break;

            // ...
        default:
            sectionName = @"Error";
            break;
    }
    return sectionName;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    DetalleEventosViewController* destinationViewController = segue.destinationViewController;
    destinationViewController.dirPagina = [[promos objectAtIndex:indexPath.row] objectForKey:@"id"];
    destinationViewController.titulo = [[promos objectAtIndex:indexPath.row] objectForKey:@"titulo"];
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
