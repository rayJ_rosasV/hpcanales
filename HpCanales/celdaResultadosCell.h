//
//  celdaResultadosCell.h
//  hpcanales
//
//  Created by Raymundo Julian on 07/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface celdaResultadosCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UILabel *tintalaser;
@property (weak, nonatomic) IBOutlet UILabel *numUsuers;
@property (weak, nonatomic) IBOutlet UILabel *multiImp;

@end
