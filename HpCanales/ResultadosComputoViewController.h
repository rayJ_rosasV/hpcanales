//
//  ResultadosComputoViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 03/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultadosComputoViewController : UIViewController

@property (strong, nonatomic) NSMutableArray* seleccion;
@property (strong, nonatomic) NSMutableArray* subtitulos;

@property (weak, nonatomic) IBOutlet UITableView *tabla;

@end
