//
//  PromocionesTableViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 29/11/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "PromocionesTableViewController.h"
#import "DetalleGeneralPromosViewController.h"
#import "articuloCell.h"
#import "DataManager.h"

#import "TFHpple.h"
#import "articulo.h"
//#import "DeatallePromosViewController.h"

@interface PromocionesTableViewController ()

@end




@implementation PromocionesTableViewController{

NSMutableArray *promosPrimitivo;
NSMutableArray *promos;
NSMutableArray *promosNombres;

}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"5textura.jpg"]];
    [tempImageView setFrame:self.tableView.frame];
    self.tableView.backgroundView = tempImageView;

    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    

    

    
	// Do any additional setup after loading the view.
    _tabla.contentInset = UIEdgeInsetsMake(20.0,0.0,0.0,0.0);
    DataManager* data = [[DataManager alloc]init];
    promosPrimitivo = [data cargarDatosPromos];
    promosPrimitivo = [promosPrimitivo objectAtIndex:0];
    NSLog(@"Promos primitivo: %@",promosPrimitivo);
    [self obtenerJson];
    //[NSThread detachNewThreadSelector:@selector(obtenerJson) toTarget:self withObject:nil];
    NSLog(@"arreglo editado %@",promos);
    
    [self compararCadenas];
    [data guardarDatosPromos:promosPrimitivo];


    self.tabla.delegate=self;
    self.tabla.dataSource=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Json

-(void) obtenerJson{
    
    NSError *error;
    NSDictionary *dict= [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://hp-canales.com.mx/hp_json.php?c=promociones"]] options:NSJSONReadingMutableContainers error:&error];
    promos =[dict objectForKey:@"Entradas"];

    
    
    
    [self.tabla reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Promos primitivo2: %@",promosPrimitivo);


    if (!promosPrimitivo) {
        //NSLog(@"El arreglo esta vacio");
    }
    return [promosPrimitivo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cadena= @"promociones";
    articuloCell *celda= [tableView dequeueReusableCellWithIdentifier:cadena];
    
    if (!celda) {
        celda= [[articuloCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cadena];
    }
    
    if (promosPrimitivo) {
        if (indexPath.row < [promosPrimitivo count]) {
            celda.titulo.text=[[promos objectAtIndex:indexPath.row] objectForKey:@"titulo"];
            celda.imagen.image =  [UIImage imageNamed:@"5imagen.jpg"];
        }
    }
    
    
    return celda;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DetalleGeneralPromosViewController* destinationViewController = segue.destinationViewController;
        destinationViewController.dirPagina = [[promosPrimitivo objectAtIndex:indexPath.row] objectForKey:@"id"];

}

-(void) compararCadenas{
    
    if (promos==NULL) {
        promos=promosPrimitivo;
    }
    else if ([promosPrimitivo isEqualToArray:promos] ){
        NSLog(@"iguales");
    }
    else{
        promosPrimitivo=promos;
    }
}

- (IBAction)menuBoton:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}
@end
