//
//  CollectionViewController.m
//  HpCanales
//
//  Created by Raymundo Julian on 17/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import "CollectionViewController.h"
#import "collectionViewCell.h"
#import "DetalleNotViewController.h"

@interface CollectionViewController (){
    NSMutableArray* titulares;
    NSMutableArray *titutalesNombres;
    NSMutableArray* titularesPrimitivo;
}

@end

@implementation CollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.


    [[self coleccion]setDataSource:self];
    [[self coleccion]setDelegate:self];

    [NSThread detachNewThreadSelector:@selector(obtenerJson) toTarget:self withObject:nil];
    [self.coleccion setUserInteractionEnabled:YES];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [titulares count];
}
    
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* idCelda= @"celda";
    collectionViewCell* celda = [collectionView dequeueReusableCellWithReuseIdentifier:idCelda forIndexPath:indexPath];
    celda.texto.text = [[titulares objectAtIndex:indexPath.row]objectForKey:@"titulo"];
    celda.texto.numberOfLines = 0;
    [celda.texto sizeToFit];
    celda.texto.textAlignment = NSTextAlignmentCenter;
    
    return celda;
}

#pragma mark - Json

-(void) obtenerJson{
    
    NSError *error;
    NSDictionary *dict= [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://hp-canales.com.mx/hp_json.php?c=notas-de-interes"]] options:NSJSONReadingMutableContainers error:&error];
    titulares=[dict objectForKey:@"Entradas"];
    NSLog(@"Notas: %@",dict);
    
    
    
    
    [self.coleccion reloadData];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *indexPath = [self.coleccion indexPathForCell:sender];
    DetalleNotViewController* destinationViewController = segue.destinationViewController;
    destinationViewController.dirPagina = [[titulares objectAtIndex:indexPath.row] objectForKey:@"id"];
    
}

//-(void) compararCadenas{
//    
//    if (promos==NULL) {
//        promos=promosPrimitivo;
//    }
//    else if ([promosPrimitivo isEqualToArray:promos] ){
//        NSLog(@"iguales");
//    }
//    else{
//        promosPrimitivo=promos;
//    }
//}
@end
