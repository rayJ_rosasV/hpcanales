//
//  MatrizComputoViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 03/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatrizComputoViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *pickerProducto;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerFamilia;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerChasis;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerPantalla;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerProcesador;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerGeneracionModelo;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerRAM;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerDiscoDuro;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerSO;


@end

