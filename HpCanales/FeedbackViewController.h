//
//  FeedbackViewController.h
//  HpCanales
//
//  Created by Raymundo Julian on 09/12/13.
//  Copyright (c) 2013 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController
- (IBAction)atras:(UIButton *)sender;
- (IBAction)enviar:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextView *campoTexto;
- (IBAction)cancelar:(UIButton *)sender;

@end
