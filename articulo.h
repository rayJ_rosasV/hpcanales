//
//  articulo.h
//  hpcanales
//
//  Created by Raymundo Julian on 13/02/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface articulo : NSObject
@property (nonatomic, copy) NSString *descripcion;
@property (nonatomic, copy) NSString *url;

@end
