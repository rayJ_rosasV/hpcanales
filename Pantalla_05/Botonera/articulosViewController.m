//
//  articulosViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 28/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "articulosViewController.h"
#import "articuloCell.h"
#import "DetalleNotViewController.h"
#import "DataManager.h"
#import "TFHpple.h"
#import "Tutorial.h"

@interface articulosViewController (){
    
    
    NSMutableArray *promosPrimitivo;
    NSMutableArray *promos;
    NSMutableArray *promosNombres;
    NSMutableArray *_objects;
    NSMutableArray* img;

    
}

@end

@implementation articulosViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (NSString*)loadTutorials: (NSString*) post {
    // 1
    NSString* dir= [NSString stringWithFormat:@"http://hp-canales.com.mx/?p=%@",post];
    [dir stringByAppendingString:post];
    NSLog(@"DIR: %@",dir);
    NSURL *tutorialsUrl = [NSURL URLWithString:dir];
    NSData *tutorialsHtmlData = [NSData dataWithContentsOfURL:tutorialsUrl];
    
    // 2
    TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:tutorialsHtmlData];
    
    // 3
    NSString *tutorialsXpathQueryString = @"//section[@class='post-content']/p/a";
    NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
    
    // 4
    NSMutableArray *newTutorials = [[NSMutableArray alloc] initWithCapacity:0];
    Tutorial* tut = [[Tutorial alloc]init];
    tut.url = [[tutorialsNodes objectAtIndex:0] objectForKey:@"href"];
    NSLog(@"resultado final: %@",tut.url);
    // 8
    _objects = newTutorials;
//    [self.tableView reloadData];
    return tut.url;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabla.delegate=self;
    self.tabla.dataSource=self;

    


    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"5textura.jpg"]];
    [tempImageView setFrame:self.tableView.frame];
    
    
    self.tableView.backgroundView = tempImageView;
    
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    
	// Do any additional setup after loading the view.
    _tabla.contentInset = UIEdgeInsetsMake(20.0,0.0,0.0,0.0);

    NSLog(@"Promos: %@",promos);
    [self obtenerJson];
    //[NSThread detachNewThreadSelector:@selector(obtenerJson) toTarget:self withObject:nil];
    int i = 0;
    NSLog(@"arreglo editado %@",promos);
    for (NSDictionary* dict in promos) {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self loadTutorials:[[dict objectForKey:@"id"]]]];
        NSLog(@"dicti: %@",dict);
        NSLog(@"id= %@",[dict objectForKey:@"id"]);
        NSString* image = [[NSString alloc]init];
        image= [dict objectForKey:@"id"];
        NSLog(@"image= %@",image);
        
        [img addObject:image];
    }
    NSLog(@"img: %@",img);
    
    
//    [self compararCadenas];


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tabla reloadData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    
    

    return [promos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cadena= @"articulo";
    articuloCell *celda= [tableView dequeueReusableCellWithIdentifier:cadena];
    
//    if (!celda) {
//        celda= [[articuloCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cadena];
//    }
    
    if (promos) {
        if (indexPath.row < [promos count]) {
            celda.titulo.text=[[promos objectAtIndex:indexPath.row] objectForKey:@"titulo"];
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self loadTutorials:[[promos objectAtIndex:indexPath.row] objectForKey:@"id"]]]]];
            celda.imagen.image = image;


 
        }
    }
    
    
    return celda;
}


#pragma mark - Json

-(void) obtenerJson{
    
    NSError *error;
    NSDictionary *dict= [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://hp-canales.com.mx/hp_json.php?c=notas-de-interes"]] options:NSJSONReadingMutableContainers error:&error];
    promos =[dict objectForKey:@"Entradas"];
    
    
    
    
    [self.tabla reloadData];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    DetalleNotViewController* destinationViewController = segue.destinationViewController;
    destinationViewController.dirPagina = [[promos objectAtIndex:indexPath.row] objectForKey:@"id"];
    
}
- (IBAction)menuBoton:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
