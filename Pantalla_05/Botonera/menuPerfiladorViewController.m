//
//  menuPerfiladorViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 28/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "menuPerfiladorViewController.h"

@interface menuPerfiladorViewController ()

@end

@implementation menuPerfiladorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    UIImage *image = [UIImage imageNamed:@"9pleca_azul.jpg"];
    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
