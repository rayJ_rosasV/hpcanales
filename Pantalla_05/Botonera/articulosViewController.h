//
//  articulosViewController.h
//  hpcanales
//
//  Created by Raymundo Julian on 28/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface articulosViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *tabla;
- (IBAction)menuBoton:(UIBarButtonItem *)sender;

@end
