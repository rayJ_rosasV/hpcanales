//
//  tabbarViewController.m
//  hpcanales
//
//  Created by Raymundo Julian on 07/01/14.
//  Copyright (c) 2014 Raymundo Julian. All rights reserved.
//

#import "tabbarViewController.h"

@interface tabbarViewController ()

@end


@implementation tabbarViewController

- (id)init {
    
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];

    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.tabBarController.selectedIndex = 2;



    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1];
    shadow.shadowOffset = CGSizeMake(0, 0);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:@"HelveticaNeue" size:28.0], NSFontAttributeName, nil]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    if ([_opcionInicio isEqualToString:@"articulos"]) {
    }
//    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    UIImage *tabBarBackground = [UIImage imageNamed:@"botones.jpg"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    UITabBarItem *item0 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:3];
    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:4];

    item0.image = [[UIImage imageNamed:@"8boton_articulos.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item0.selectedImage = [UIImage imageNamed:@"8boton_articulos.png"];
    item1.image = [[UIImage imageNamed:@"8boton_promos.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item1.selectedImage = [UIImage imageNamed:@"8boton_promos.png"];
    item2.image = [[UIImage imageNamed:@"8boton_configurador"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item2.selectedImage = [UIImage imageNamed:@"8boton_configurador"];
    item3.image = [[UIImage imageNamed:@"8boton_eventos.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item3.selectedImage = [UIImage imageNamed:@"8boton_eventos.png"];
    item4.image = [[UIImage imageNamed:@"8boton_donde_comprar.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item4.selectedImage = [UIImage imageNamed:@"8boton_donde_comprar.png"];



    NSLog(@"opcion de inicio: %@",_opcionInicio);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
